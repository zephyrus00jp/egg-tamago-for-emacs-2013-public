つぎが一番重要なメモ
egg-script.txt : 2013年4月に emacs 24.3 で入力できなくなり、
	       script で記録した make clean; make の
	       出力の中の警告をもとに、修正を加えていったが、その記録に
	       コメントを付け加えて備忘録にしたもの。
	       
あとはおまけ

emacs-23-egg-problem.txt：2010年に最終変更したメモだが、その当時にすで
	に大きなpatch を加えないと使えないことがわかる。

patch-history.txt： 2013 年4月にパッチ作業したときの最後の方の変更分の
		    警告と対応完了のメモ。

egg-patch.txt： make-coding-system が define-coding-system と変更され
		た頃の大きな変更。（2007年に作成されたパッチである。）

personal-dic.txt: 以前 ICOT が公開していた形態素辞書から作った辞書を
		  有効にするためのメモ

jserverrc-with-icot-dict: 上記 icot 辞書を有効にする際に必要な
			  jserverrcのコピー
